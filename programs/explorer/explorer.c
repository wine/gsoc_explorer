/*
 * explorer.exe
 *
 * Copyright 2004 CodeWeavers, Mike Hearn
 * Copyright 2005,2006 CodeWeavers, Aric Stewart
 * Copyright 2011 Jay Yang
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#define COBJMACROS

#include "wine/unicode.h"
#include "wine/debug.h"
#include "explorer_private.h"
#include "resource.h"

#include <initguid.h>
#include <windows.h>
#include <windowsx.h>
#include <shobjidl.h>
#include <shlobj.h>
#include <commoncontrols.h>


WINE_DEFAULT_DEBUG_CHANNEL(explorer);

#define EXPLORER_INFO_INDEX 0

#define NAVBAR_HEIGHT 24

#define DEFAULT_WIDTH 640
#define DEFAULT_HEIGHT 480

#define BACK_BUTTON_INDEX 0
#define FORWARD_BUTTON_INDEX 1
#define UP_BUTTON_INDEX 28

/*From shell32*/
#define IDB_TB_LARGE_LIGHT 214


static const WCHAR EXPLORER_CONTROL_CLASS[] = {'W','I','N','E','_','E','X','P','L','O','R','E','R','_','C','O','N','T','R','O','L','\0'};
static const WCHAR EXPLORER_CLASS[] = {'W','I','N','E','_','E','X','P','L','O','R','E','R','\0'};
static const WCHAR BACK_BUTTON_NAME[] = {'B','a','c','k','\0'};
static const WCHAR FORWARD_BUTTON_NAME[] = {'F','o','r','w','a','r','d','\0'};
static const WCHAR UP_BUTTON_NAME[] = {'U','p','\0'};
static const WCHAR PATH_BOX_NAME[] = {'\0'};

static HICON back_button_icon, forward_button_icon, up_button_icon;

HINSTANCE explorer_hInstance;

typedef struct parametersTAG {
    BOOL    explorer_mode;
    WCHAR   root[MAX_PATH];
    WCHAR   selection[MAX_PATH];
} parameters_struct;

typedef struct
{
    IExplorerBrowser *browser;
    HWND back_button,forward_button,up_button,path_box,go_button;
} explorer_info;

typedef struct
{
    IExplorerBrowserEvents IExplorerBrowserEvents_iface;
    explorer_info* info;
    LONG ref;
} IExplorerBrowserEventsImpl;

enum
{
    BACK_BUTTON,FORWARD_BUTTON,UP_BUTTON,GO_BUTTON
};

static IExplorerBrowserEventsImpl *impl_from_IExplorerBrowserEvents(IExplorerBrowserEvents *iface)
{
    return CONTAINING_RECORD(iface, IExplorerBrowserEventsImpl, IExplorerBrowserEvents_iface);
}

static HRESULT WINAPI IExplorerBrowserEventsImpl_fnQueryInterface(IExplorerBrowserEvents *iface, REFIID riid, void **ppvObject)
{
    return E_NOINTERFACE;
}

static ULONG WINAPI IExplorerBrowserEventsImpl_fnAddRef(IExplorerBrowserEvents *iface)
{
    IExplorerBrowserEventsImpl *This = impl_from_IExplorerBrowserEvents(iface);
    return InterlockedIncrement(&This->ref);
}

static ULONG WINAPI IExplorerBrowserEventsImpl_fnRelease(IExplorerBrowserEvents *iface)
{
    IExplorerBrowserEventsImpl *This = impl_from_IExplorerBrowserEvents(iface);
    ULONG ref = InterlockedDecrement(&This->ref);
    if(!ref)
        HeapFree(GetProcessHeap(),0,This);
    return ref;
}

static HRESULT WINAPI IExplorerBrowserEventsImpl_fnOnNavigationComplete(IExplorerBrowserEvents *iface, PCIDLIST_ABSOLUTE pidl)
{
    IExplorerBrowserEventsImpl *This = impl_from_IExplorerBrowserEvents(iface);
    WCHAR path[MAX_PATH];
    if(SHGetPathFromIDListW(pidl,path))
        SetWindowTextW(This->info->path_box,path);
    return S_OK;
}

static HRESULT WINAPI IExplorerBrowserEventsImpl_fnOnNavigationFailed(IExplorerBrowserEvents *iface, PCIDLIST_ABSOLUTE pidl)
{
    return S_OK;
}

static HRESULT WINAPI IExplorerBrowserEventsImpl_fnOnNavigationPending(IExplorerBrowserEvents *iface, PCIDLIST_ABSOLUTE pidl)
{
    return S_OK;
}

static HRESULT WINAPI IExplorerBrowserEventsImpl_fnOnViewCreated(IExplorerBrowserEvents *iface, IShellView *psv)
{
    return S_OK;
}

static IExplorerBrowserEventsVtbl vt_IExplorerBrowserEvents =
{
    IExplorerBrowserEventsImpl_fnQueryInterface,
    IExplorerBrowserEventsImpl_fnAddRef,
    IExplorerBrowserEventsImpl_fnRelease,
    IExplorerBrowserEventsImpl_fnOnNavigationPending,
    IExplorerBrowserEventsImpl_fnOnViewCreated,
    IExplorerBrowserEventsImpl_fnOnNavigationComplete,
    IExplorerBrowserEventsImpl_fnOnNavigationFailed
};

static IExplorerBrowserEvents *make_explorer_events(explorer_info *info)
{
    IExplorerBrowserEventsImpl *ret
        = HeapAlloc(GetProcessHeap(), 0, sizeof(IExplorerBrowserEventsImpl));
    ret->IExplorerBrowserEvents_iface.lpVtbl = &vt_IExplorerBrowserEvents;
    ret->info = info;
    IExplorerBrowserEvents_AddRef(&ret->IExplorerBrowserEvents_iface);
    return &ret->IExplorerBrowserEvents_iface;
}

static void load_navbar_icons(void)
{
    HINSTANCE shell32_hInstance = GetModuleHandleA("shell32.dll");
    IImageList* navbar_icons;
    HIMAGELIST icons_handle
        = ImageList_LoadImageW(shell32_hInstance,
                               (LPCWSTR)MAKEINTRESOURCE(IDB_TB_LARGE_LIGHT),
                               NAVBAR_HEIGHT,0,CLR_NONE,IMAGE_BITMAP,0);
    HIMAGELIST_QueryInterface(icons_handle,&IID_IImageList,(void**)&navbar_icons);
    IImageList_GetIcon(navbar_icons,BACK_BUTTON_INDEX,0,&back_button_icon);
    IImageList_GetIcon(navbar_icons,FORWARD_BUTTON_INDEX,0,&forward_button_icon);
    IImageList_GetIcon(navbar_icons,UP_BUTTON_INDEX,0,&up_button_icon);
}

static void make_explorer_window(IShellFolder* startFolder)
{
    RECT explorerRect;
    HWND window;
    FOLDERSETTINGS fs;
    IExplorerBrowserEvents *events;
    explorer_info *info;
    HRESULT hres;
    DWORD cookie;
    WCHAR explorer_title[100];
    WCHAR go_button_name[100];

    LoadStringW(explorer_hInstance,IDS_EXPLORER_TITLE,explorer_title,
                sizeof(explorer_title)/sizeof(WCHAR));
    LoadStringW(explorer_hInstance,IDS_EXPLORER_GO,go_button_name,
                sizeof(go_button_name)/sizeof(WCHAR));
    info = HeapAlloc(GetProcessHeap(),0,sizeof(explorer_info));
    if(!info)
    {
        WINE_ERR("Could not allocate a explorer_info struct\n");
        return;
    }
    hres = CoCreateInstance(&CLSID_ExplorerBrowser,NULL,CLSCTX_INPROC_SERVER,
                            &IID_IExplorerBrowser,(LPVOID*)&info->browser);
    if(!SUCCEEDED(hres))
    {
        WINE_ERR("Could not obtain an instance of IExplorerBrowser\n");
        HeapFree(GetProcessHeap(),0,info);
        return;
    }

    window = CreateWindowW(EXPLORER_CLASS,explorer_title,WS_OVERLAPPEDWINDOW,
                           CW_USEDEFAULT,CW_USEDEFAULT,DEFAULT_WIDTH,
                           DEFAULT_HEIGHT,NULL,NULL,explorer_hInstance,NULL);
    fs.ViewMode = FVM_DETAILS;
    fs.fFlags = FWF_AUTOARRANGE;
    explorerRect.left = 0;
    explorerRect.top = NAVBAR_HEIGHT;
    explorerRect.right = DEFAULT_WIDTH;
    explorerRect.bottom = DEFAULT_HEIGHT;

    IExplorerBrowser_Initialize(info->browser,window,&explorerRect,&fs);
    IExplorerBrowser_SetOptions(info->browser,EBO_SHOWFRAMES);
    events = make_explorer_events(info);
    IExplorerBrowser_Advise(info->browser,events,&cookie);
    SetWindowLongPtrW(window,EXPLORER_INFO_INDEX,(LONG)info);
    /*setup navbar*/
    info->back_button = CreateWindowW(WC_BUTTONW,BACK_BUTTON_NAME,
                                     WS_CHILD | WS_VISIBLE | BS_ICON,0,0,
                                     NAVBAR_HEIGHT,NAVBAR_HEIGHT,window,
                                     (HMENU)BACK_BUTTON,
                                      explorer_hInstance,NULL);
    info->forward_button = CreateWindowW(WC_BUTTONW,BACK_BUTTON_NAME,
                                        WS_CHILD | WS_VISIBLE | BS_ICON,
                                        NAVBAR_HEIGHT,0,NAVBAR_HEIGHT,
                                        NAVBAR_HEIGHT,window,
                                        (HMENU)FORWARD_BUTTON,
                                         explorer_hInstance,NULL);
    info->up_button = CreateWindowW(WC_BUTTONW,UP_BUTTON_NAME,
                                    WS_CHILD | WS_VISIBLE | BS_ICON,
                                    2*NAVBAR_HEIGHT,0,NAVBAR_HEIGHT,
                                    NAVBAR_HEIGHT,window,(HMENU)UP_BUTTON,
                                    explorer_hInstance,NULL);
    SendMessageW(info->back_button,BM_SETIMAGE,IMAGE_ICON,
                 (LPARAM)back_button_icon);
    SendMessageW(info->forward_button,BM_SETIMAGE,IMAGE_ICON,
                 (LPARAM)forward_button_icon);
    SendMessageW(info->up_button,BM_SETIMAGE,IMAGE_ICON,(LPARAM)up_button_icon);
    info->path_box = CreateWindowW(WC_EDITW,PATH_BOX_NAME,WS_CHILD | WS_VISIBLE,
                                  3*NAVBAR_HEIGHT,0,640-4*NAVBAR_HEIGHT,
                                  NAVBAR_HEIGHT,window,NULL,
                                   explorer_hInstance,NULL);
    info->go_button = CreateWindowW(WC_BUTTONW,go_button_name,
                                   WS_CHILD | WS_VISIBLE,640-NAVBAR_HEIGHT,0,
                                   NAVBAR_HEIGHT,NAVBAR_HEIGHT,window,
                                   (HMENU)GO_BUTTON,explorer_hInstance,NULL);
    IExplorerBrowser_BrowseToObject(info->browser,(IUnknown*)startFolder,
                                    SBSP_ABSOLUTE);
    ShowWindow(window,SW_SHOWDEFAULT);
    UpdateWindow(window);
    IExplorerBrowserEvents_Release(events);
}

static void update_window_size(explorer_info *info, int height, int width)
{
    RECT new_rect;
    int path_box_width = width-4*NAVBAR_HEIGHT;
    new_rect.left = 0;
    new_rect.top = NAVBAR_HEIGHT;
    new_rect.right = width;
    new_rect.bottom = height;
    IExplorerBrowser_SetRect(info->browser,NULL,new_rect);
    if(path_box_width<0)
        path_box_width = 0;
    SetWindowPos(info->path_box,0,3*NAVBAR_HEIGHT,0,
                 path_box_width,NAVBAR_HEIGHT,0);
    SetWindowPos(info->go_button,0,width-NAVBAR_HEIGHT,0,
                 NAVBAR_HEIGHT,NAVBAR_HEIGHT,0);
}

static void do_exit(int code)
{
    OleUninitialize();
    ExitProcess(code);
}

LRESULT CALLBACK explorer_wnd_proc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    explorer_info *info
        = (explorer_info*)GetWindowLongPtrW(hwnd,EXPLORER_INFO_INDEX);
    IExplorerBrowser *browser = NULL;

    if(info)
        browser = info->browser;
    switch(uMsg)
    {
    case WM_DESTROY:
        IExplorerBrowser_Release(browser);
        HeapFree(GetProcessHeap(),0,info);
        break;
    case WM_COMMAND:
        switch(wParam)
        {
        case BACK_BUTTON:
            IExplorerBrowser_BrowseToObject(browser,NULL,SBSP_NAVIGATEBACK);
            break;
        case FORWARD_BUTTON:
            IExplorerBrowser_BrowseToObject(browser,NULL,SBSP_NAVIGATEFORWARD);
            break;
        case UP_BUTTON:
            IExplorerBrowser_BrowseToObject(browser,NULL,SBSP_PARENT);
            break;
        case GO_BUTTON:
            {
                WCHAR path[MAX_PATH];
                PIDLIST_ABSOLUTE pidl;
                *((WORD*)path)=MAX_PATH;
                /*fix buffer overflow*/
                SendMessageW(info->path_box,EM_GETLINE,0,(LPARAM)path);
                pidl = ILCreateFromPathW(path);
                IExplorerBrowser_BrowseToIDList(browser,pidl,SBSP_ABSOLUTE);
                ILFree(pidl);
                break;
            }
        }
        break;
    case WM_SIZE:
        update_window_size(info,HIWORD(lParam),LOWORD(lParam));
        break;
    default:
        return DefWindowProcW(hwnd,uMsg,wParam,lParam);
    }
    return 0;
}

static void register_explorer_window_class(void)
{
    WNDCLASSEXW window_class;
    window_class.cbSize = sizeof(WNDCLASSEXW);
    window_class.style = 0;
    window_class.cbClsExtra = 0;
    window_class.cbWndExtra = sizeof(LONG_PTR);
    window_class.lpfnWndProc = explorer_wnd_proc;
    window_class.hInstance = explorer_hInstance;
    window_class.hIcon = NULL;
    window_class.hCursor = NULL;
    window_class.hbrBackground = NULL;
    window_class.lpszMenuName = NULL;
    window_class.lpszClassName = EXPLORER_CLASS;
    window_class.hIconSm = NULL;
    RegisterClassExW(&window_class);
}

static IShellFolder* get_starting_shell_folder(parameters_struct* params){
    IShellFolder* desktop,*folder;
    LPITEMIDLIST root_pidl;
    HRESULT hres;

    SHGetDesktopFolder(&desktop);
    if(!params->root || (strlenW(params->root)==0))
    {
        return desktop;
    }
    hres = IShellFolder_ParseDisplayName(desktop,NULL,NULL,
                                         params->root,NULL,
                                         &root_pidl,NULL);

    if(FAILED(hres))
    {
        return desktop;
    }
    hres = IShellFolder_BindToObject(desktop,root_pidl,NULL,
                                     &IID_IShellFolder,
                                     (void**)&folder);
    if(FAILED(hres))
    {
        return desktop;
    }
    IShellFolder_Release(desktop);
    return folder;
}

/*
 * The WindowProc for the control window
 */
LRESULT CALLBACK explorer_control_wnd_proc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
    case WM_COPYDATA:
        {
            PCOPYDATASTRUCT data = (PCOPYDATASTRUCT)lParam;
            parameters_struct *params = data->lpData;
            IShellFolder *folder;
            folder = get_starting_shell_folder(params);
            make_explorer_window(folder);
            IShellFolder_Release(folder);
            break;
        }
    case WM_QUIT:
        do_exit(wParam);
    default:
        return DefWindowProcW(hwnd,uMsg,wParam,lParam);
    }
    return 0;
}

static HWND make_control_window(void)
{
    WNDCLASSEXW window_class;
    window_class.cbSize = sizeof(WNDCLASSEXW);
    window_class.style = 0;
    window_class.cbClsExtra = 0;
    window_class.cbWndExtra = 0;
    window_class.lpfnWndProc = explorer_control_wnd_proc;
    window_class.hInstance = explorer_hInstance;
    window_class.hIcon = NULL;
    window_class.hCursor = NULL;
    window_class.hbrBackground = NULL;
    window_class.lpszMenuName = NULL;
    window_class.lpszClassName = EXPLORER_CONTROL_CLASS;
    window_class.hIconSm = NULL;
    RegisterClassExW(&window_class);

    return CreateWindowW(EXPLORER_CONTROL_CLASS,EXPLORER_CONTROL_CLASS,
                         0,CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT,
                         CW_USEDEFAULT,HWND_MESSAGE,NULL,
                         explorer_hInstance,NULL);
}


static int copy_path_string(LPWSTR target, LPWSTR source)
{
    INT i = 0;

    while (isspaceW(*source)) source++;

    if (*source == '\"')
    {
        source ++;
        while (*source != '\"') target[i++] = *source++;
        target[i] = 0;
        source ++;
        i+=2;
    }
    else
    {
        while (*source && !isspaceW(*source)) target[i++] = *source++;
        target[i] = 0;
    }
    return i;
}


static void copy_path_root(LPWSTR root, LPWSTR path)
{
    LPWSTR p,p2;
    INT i = 0;

    p = path;
    while (*p!=0)
        p++;

    while (*p!='\\' && p > path)
        p--;

    if (p == path)
        return;

    p2 = path;
    while (p2 != p)
    {
        root[i] = *p2;
        i++;
        p2++;
    }
    root[i] = 0;
}

/*
 * Command Line parameters are:
 * [/n]  Opens in single-paned view for each selected items. This is default
 * [/e,] Uses Windows Explorer View
 * [/root,object] Specifies the root level of the view
 * [/select,object] parent folder is opened and specified object is selected
 */
static void parse_command_line(LPWSTR commandline,parameters_struct *parameters)
{
    static const WCHAR arg_n[] = {'/','n'};
    static const WCHAR arg_e[] = {'/','e',','};
    static const WCHAR arg_root[] = {'/','r','o','o','t',','};
    static const WCHAR arg_select[] = {'/','s','e','l','e','c','t',','};
    static const WCHAR arg_desktop[] = {'/','d','e','s','k','t','o','p'};

    LPWSTR p, p2;

    p2 = commandline;
    p = strchrW(commandline,'/');
    while(p)
    {
        if (strncmpW(p, arg_n, sizeof(arg_n)/sizeof(WCHAR))==0)
        {
            parameters->explorer_mode = FALSE;
            p += sizeof(arg_n)/sizeof(WCHAR);
        }
        else if (strncmpW(p, arg_e, sizeof(arg_e)/sizeof(WCHAR))==0)
        {
            parameters->explorer_mode = TRUE;
            p += sizeof(arg_e)/sizeof(WCHAR);
        }
        else if (strncmpW(p, arg_root, sizeof(arg_root)/sizeof(WCHAR))==0)
        {
            p += sizeof(arg_root)/sizeof(WCHAR);
            p+=copy_path_string(parameters->root,p);
        }
        else if (strncmpW(p, arg_select, sizeof(arg_select)/sizeof(WCHAR))==0)
        {
            p += sizeof(arg_select)/sizeof(WCHAR);
            p+=copy_path_string(parameters->selection,p);
            if (!parameters->root[0])
                copy_path_root(parameters->root,
                               parameters->selection);
        }
        else if (strncmpW(p, arg_desktop, sizeof(arg_desktop)/sizeof(WCHAR))==0)
        {
            p += sizeof(arg_desktop)/sizeof(WCHAR);
            manage_desktop( p );  /* the rest of the command line is handled by desktop mode */
        }
        else p++;

        p2 = p;
        p = strchrW(p,'/');
    }
    if (p2 && *p2)
    {
        /* left over command line is generally the path to be opened */
        copy_path_string(parameters->root,p2);
    }
}

int WINAPI wWinMain(HINSTANCE hinstance,
                    HINSTANCE previnstance,
                    LPWSTR cmdline,
                    int cmdshow)
{
    parameters_struct   parameters;
    HRESULT hres;
    MSG msg;
    HWND control;
    COPYDATASTRUCT data;
    HANDLE mutex;

    memset(&parameters,0,sizeof(parameters));
    memset(&data,0,sizeof(data));
    explorer_hInstance = hinstance;
    parse_command_line(cmdline,&parameters);
    data.lpData = &parameters;
    data.cbData = sizeof(parameters);
    /*Take a named mutex*/
    mutex = CreateMutexW(NULL,TRUE,EXPLORER_CLASS);
    if(!mutex)
    {
        WINE_WARN("Could not create a mutex\n");
    }
    else if(GetLastError()==ERROR_ALREADY_EXISTS)
    {
        WaitForSingleObject(mutex,INFINITE);
        control = FindWindowExW(HWND_MESSAGE,NULL,NULL,EXPLORER_CONTROL_CLASS);
        ReleaseMutex(mutex);
        if(control)
        {
            SendMessageW(control,WM_COPYDATA,0,(LPARAM)&data);
            ExitProcess(EXIT_SUCCESS);
        }
        else
        {
            WINE_ERR("Previous Explorer process did not shutdown properly\n");
            ExitProcess(EXIT_FAILURE);
        }
    }
    hres = OleInitialize(NULL);
    if(!SUCCEEDED(hres))
    {
        WINE_ERR("Could not initialize COM\n");
        if(mutex)
            ReleaseMutex(mutex);
        ExitProcess(EXIT_FAILURE);
    }
    load_navbar_icons();
    register_explorer_window_class();
    control = make_control_window();
    if(mutex)
        ReleaseMutex(mutex);
    SendMessageW(control,WM_COPYDATA,0,(LPARAM)&data);
    while(GetMessageW( &msg, NULL, 0, 0 ) != 0)
    {
	    TranslateMessage(&msg);
	    DispatchMessageW(&msg);
    }
    return 0;
}
